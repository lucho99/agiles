import static org.junit.Assert.*;

import org.junit.Test;


public class TestEjercicio {

	@Test
	public void testEjercicio() {
		Ejercicio ej = new Ejercicio();
		assertNotEquals(ej.dameAlgo("hola"), "hola");
		assertEquals(ej.dameAlgo("hola"), "hola te lo devuelvo.");
		assertEquals(ej.dameAlgo(""), " te lo devuelvo.");
		assertNotEquals(ej.dameAlgo(""), "");
		assertEquals(ej.dameAlgo("test 2"), "test 2 te lo devuelvo.");
	}

}
